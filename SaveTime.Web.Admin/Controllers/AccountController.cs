﻿using Ninject;
using SaveTime.DataModel.Organization;
using SaveTime.Web.Admin.Repo;
using SaveTime.Web.Admin.Repo.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveTime.Web.Admin.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IRepository<Account> _repository;

        public AccountController()
        {
            _repository = kernel.Get<IRepository<Account>>();
        }

        // GET: Account
        public ActionResult Index()
        {
            _repository.Create(new Account());

            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Account account)
        {
            _repository.Create(account);
            return View();
        }
    }
}