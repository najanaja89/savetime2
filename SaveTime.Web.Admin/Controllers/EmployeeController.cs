﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SaveTime.DataAccess;
using SaveTime.DataModel.Organization;
using SaveTime.Web.Admin.Repo;
using Ninject;
using System.Collections.Generic;
using SaveTime.Web.Admin.Models;
using AutoMapper;
using SaveTime.Web.Admin.Models.Employee;
using SaveTime.Web.Admin.Models.Branch;
using SaveTime.Web.Admin.Models.Company;
using SaveTime.Web.Admin.Models.Account;

namespace SaveTime.Web.Admin.Controllers
{
    public class EmployeeController : BaseController
    {
        private readonly IRepository<Employee> _repositoryEmployee;
        private readonly IRepository<Account> _repositoryAccount;
        private readonly IRepository<Branch> _repositoryBranch;

        IMapper _mapper;

        public EmployeeController()
        {
            _repositoryEmployee = kernel.Get<IRepository<Employee>>();
            _repositoryAccount = kernel.Get<IRepository<Account>>();
            _repositoryBranch = kernel.Get<IRepository<Branch>>();

            var config = new MapperConfiguration(
          c =>
          {
              c.CreateMap<Employee, EmployeeDetailsViewModel>();
              c.CreateMap<Branch, BranchDetailsViewModel>();
              c.CreateMap<Account, AccountDetailsViewModel>();
              c.CreateMap<Company, CompanyDetailsViewModel>();
          }
          );
            _mapper = config.CreateMapper();
        }

        public ActionResult Index()
        {
            var employees = _repositoryEmployee.GetAll();
            var employeesViewModel = new List<CustomEmployeeViewModel>();

            foreach (var emp in employees)
            {
                var evm = new CustomEmployeeViewModel()
                {
                    EmployeeId = emp.Id,
                    EmployeeName = emp.Name,
                    Phone = emp.Account.Phone,
                    Email = emp.Account.Email,
                    //CompanyName = emp.Branch.Company.Name,
                    BranchAddress = emp.Branch.Address
                };

                employeesViewModel.Add(evm);
            }

            return View(employeesViewModel);
        }

        public ActionResult Create()
        {
            var accounts = _repositoryAccount.GetAll();
            var branches = _repositoryBranch.GetAll();

            var bvmList = new List<BranchDetailsViewModel>();
            var avmList = new List<AccountDetailsViewModel>();

            foreach (var branch in branches)
            {
                var bvm = _mapper.Map<BranchDetailsViewModel>(branch);
                bvmList.Add(bvm);
            }

            foreach (var account in accounts)
            {
                var avm = _mapper.Map<AccountDetailsViewModel>(account);
                avmList.Add(avm);
            }

            var customEmployeeViewModel = new CustomEmployeeViewModel
            {
                Accounts = avmList,
                Branches = bvmList
            };

            return View(customEmployeeViewModel);
        }

        // POST: Company/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomEmployeeViewModel customEmployeeViewModel)
        {
            if (ModelState.IsValid && customEmployeeViewModel != null)
            {
                var e = new Employee()
                {
                    Name = customEmployeeViewModel.EmployeeName,
                    Branch = new Branch { Address = customEmployeeViewModel.SelectedBranchAddress, Phone = customEmployeeViewModel.SelectedBranchPhone },
                    Account = new Account { Email = customEmployeeViewModel.SelectedAccountEmail, Phone = customEmployeeViewModel.SelectedAccountPhone },

                };
                _repositoryEmployee.Create(e);
                return RedirectToAction("Index");
            }

            return View(customEmployeeViewModel);
        }


        // GET: Company/Edit/5
        public ActionResult Edit(int id)
        {
            var accounts = _repositoryAccount.GetAll();
            var branches = _repositoryBranch.GetAll();

            var bvmList = new List<BranchDetailsViewModel>();
            var avmList = new List<AccountDetailsViewModel>();

            foreach (var branch in branches)
            {
                var bvm = _mapper.Map<BranchDetailsViewModel>(branch);
                bvmList.Add(bvm);
            }

            foreach (var account in accounts)
            {
                var avm = _mapper.Map<AccountDetailsViewModel>(account);
                avmList.Add(avm);
            }

            var customEmployeeViewModel = new CustomEmployeeViewModel
            {
                Accounts = avmList,
                Branches = bvmList
            };

            var employee = _repositoryEmployee.GetById(id);
            customEmployeeViewModel.EmployeeId = employee.Id;
            customEmployeeViewModel.EmployeeName = employee.Name;
            customEmployeeViewModel.Phone = employee.Account.Phone;
            customEmployeeViewModel.Email = employee.Account.Email;
            customEmployeeViewModel.BranchAddress = employee.Branch.Address;
            customEmployeeViewModel.BranchId = employee.Branch.Id;
            customEmployeeViewModel.AccountId = employee.Account.Id;

            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(customEmployeeViewModel);
        }

        // POST: Company/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomEmployeeViewModel customEmployeeViewModel)
        {
            if (ModelState.IsValid)
            {
                var e = new Employee()
                {
                    Id = customEmployeeViewModel.EmployeeId,
                    Name = customEmployeeViewModel.EmployeeName,
                    Branch = new Branch { Id = customEmployeeViewModel.BranchId, Address = customEmployeeViewModel.SelectedBranchAddress, Phone = customEmployeeViewModel.SelectedBranchPhone },
                    Account = new Account { Id = customEmployeeViewModel.AccountId, Email = customEmployeeViewModel.SelectedAccountEmail, Phone = customEmployeeViewModel.SelectedAccountPhone },

                };

                _repositoryEmployee.Edit(e);
                return RedirectToAction("Index");
            }
            return View(customEmployeeViewModel);
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int id)
        {
            var employee = _repositoryEmployee.GetById(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = _repositoryEmployee.GetById(id);
            _repositoryEmployee.Remove(employee);
            return RedirectToAction("Index");
        }

    }
}
