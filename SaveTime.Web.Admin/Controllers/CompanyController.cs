﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ninject;
using SaveTime.DataAccess;
using SaveTime.DataModel.Organization;
using SaveTime.Web.Admin.Models.Branch;
using SaveTime.Web.Admin.Models.Company;
using SaveTime.Web.Admin.Models.Employee;
using SaveTime.Web.Admin.Repo;

namespace SaveTime.Web.Admin.Controllers
{
    public class CompanyController : BaseController
    {
        readonly IRepository<Company> _repository;
        IMapper _mapper;

        public CompanyController()
        {
            _repository = kernel.Get<IRepository<Company>>();
            var config = new MapperConfiguration(
                c => 
                {
                    c.CreateMap<Employee, EmployeeDetailsViewModel>();
                    c.CreateMap<Branch, BranchDetailsViewModel>();
                    c.CreateMap<Company, CompanyDetailsViewModel>();
                }
                );
            _mapper = config.CreateMapper();
        }
        // GET: Company
        public ActionResult Index()
        {
            IEnumerable<Company> companies = _repository.GetAll();
            var company = new CompanyViewModel();

            foreach (var com in companies)
            {
                var cvm = _mapper.Map<CompanyDetailsViewModel>(com);
                company.Companies.Add(cvm);
            }

            return View(company);
        }

        // GET: Company/Details/5
        public ActionResult Details(int id)
        {

            Company company = _repository.GetById(id);
            if (company == null) return HttpNotFound();

            var companyDetails = _mapper.Map<CompanyDetailsViewModel>(company);

            //var companyDetails = new CompanyDetailsViewModel()
            //{
            //    Id = company.Id,
            //    Name = company.Name,
            //    City = company.City
            //};

            var branchDetails = new List<BranchDetailsViewModel>();

            foreach (var branch in company.Branches)
            {
                var branchDetail = _mapper.Map<BranchDetailsViewModel>(branch);
                //var branchDetail = new BranchDetailsViewModel
                //{
                //    Id = branch.Id,
                //    Address = branch.Address,
                //    Email = branch.Email,
                //    Phone = branch.Phone,
                //    StartWork = branch.StartWork,
                //    EndWork = branch.EndWork,
                //    StepWork = branch.StepWork
                //};

                IList<EmployeeDetailsViewModel> employeeDetails = new List<EmployeeDetailsViewModel>();
                foreach (var employee in branch.Employees)
                {

                    var employeeDetail = _mapper.Map<EmployeeDetailsViewModel>(employee);
                    //EmployeeDetailsViewModel employeeDetail = new EmployeeDetailsViewModel
                    //{
                    //    Id = employee.Id,
                    //    Name = employee.Name
                    //};
                    employeeDetails.Add(employeeDetail);
                }
                branchDetail.Employees = employeeDetails;
                branchDetails.Add(branchDetail);

            }
            companyDetails.Branches = branchDetails;

            return View(companyDetails);
        }

        // GET: Company/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Company/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public JsonResult Create(CreateCompanyViewModel createCompanyViewModel)
        {
            if (ModelState.IsValid && createCompanyViewModel!=null)
            {
                var c = new Company()
                {
                    Name = createCompanyViewModel.Name,
                    City = createCompanyViewModel.City
                };
                _repository.Create(c);
                return Json(c, JsonRequestBehavior.AllowGet);
            }

            return Json(createCompanyViewModel, JsonRequestBehavior.AllowGet);
        }

        // GET: Company/Edit/5
        public ActionResult Edit()
        {   
            return View();
        }

        // POST: Company/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Edit(CreateCompanyViewModel createCompanyViewModel) 
        {
            if (ModelState.IsValid && createCompanyViewModel != null)
            {
                var company = _repository.GetById(createCompanyViewModel.Id);

               company.Id = createCompanyViewModel.Id;
                company.Name = createCompanyViewModel.Name;
                company.City = createCompanyViewModel.City;

                _repository.Edit(company);
                return Json(company, JsonRequestBehavior.AllowGet);
            }
                return Json(createCompanyViewModel, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetCompanies()
        {
            IEnumerable<Company> companies = _repository.GetAll();
            var companyVM = new List<CompanyDetailsViewModel>();

            foreach (var com in companies)
            {
                var cvm = _mapper.Map<CompanyDetailsViewModel>(com);
                companyVM.Add(cvm);
            }
            return Json(companyVM, JsonRequestBehavior.AllowGet);
        }

        //// GET: Company/Edit/5
        //public ActionResult Edit(int?id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    if (company == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(company);
        //}

    
        //// GET: Company/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Company company = db.Companies.Find(id);
        //    if (company == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(company);
        //}

        //// POST: Company/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Company company = db.Companies.Find(id);
        //    db.Companies.Remove(company);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
