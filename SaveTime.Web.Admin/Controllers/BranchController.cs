﻿using AutoMapper;
using SaveTime.DataAccess;
using SaveTime.DataModel.Organization;
using SaveTime.Web.Admin.Models.Branch;
using SaveTime.Web.Admin.Models.Company;
using SaveTime.Web.Admin.Repo;
using SaveTime.Web.Admin.Repo.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveTime.Web.Admin.Controllers
{
    public class BranchController : BaseController
    {
        private readonly IRepository<Branch> _repositoryBranch = new Repository<Branch>();
        private readonly IRepository<Company> _repositoryCompany = new Repository<Company>();
        IMapper _mapper;

        public BranchController()
        {
            var config = new MapperConfiguration(
                    c =>
                    {
                        c.CreateMap<Branch, BranchDetailsViewModel>();
                        c.CreateMap<Company, CompanyDetailsViewModel>();
                    }
                    );
            _mapper = config.CreateMapper();

        }
        public ActionResult Index()
        {
            var branches = _repositoryBranch.GetAll();
            var branch = new BranchViewModel();
            foreach (var branchItem in branches)
            {
                var bvm = _mapper.Map<BranchDetailsViewModel>(branchItem);
                branch.Branches.Add(bvm);
            }

            return View(branch);
        }

        [HttpGet]
        public ActionResult Create()
        {
            //var branch = new BranchViewModel();
            //var companies = _repositoryCompany.GetAll();
            //foreach (var company in companies)
            //{
            //    var cvm = _mapper.Map<CompanyDetailsViewModel>(company);
            //    branch.CreateBranch.Companies.Add(cvm);
            //}
            return View();
        }



        public JsonResult GetCompanies()
        {
            IEnumerable<Company> companies = _repositoryCompany.GetAll();
            var companyVM = new List<CompanyDetailsViewModel>();

            foreach (var com in companies)
            {
                var cvm = _mapper.Map<CompanyDetailsViewModel>(com);
                companyVM.Add(cvm);
            }
            return Json(companyVM, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Create(CreateBranchViewModel createBranch)
        {

            var branch = new Branch()
            {
                Address = createBranch.Address,
                Email = createBranch.Email,
                StartWork = createBranch.StartWork,
                EndWork = createBranch.EndWork,
                Phone = createBranch.Phone,
                StepWork = createBranch.StepWork
            };
            _repositoryBranch.Create(branch);
            return Json(branch);
        }
    }
}