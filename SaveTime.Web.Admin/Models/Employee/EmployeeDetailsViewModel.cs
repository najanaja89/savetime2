﻿namespace SaveTime.Web.Admin.Models.Employee
{
    public class EmployeeDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}