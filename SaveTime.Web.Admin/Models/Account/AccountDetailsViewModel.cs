﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveTime.Web.Admin.Models.Account
{
    public class AccountDetailsViewModel
    {
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}