﻿using SaveTime.Web.Admin.Models.Company;
using SaveTime.Web.Admin.Models.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveTime.Web.Admin.Models.Branch
{
    public class BranchDetailsViewModel
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public DateTime StartWork { get; set; }
        public DateTime EndWork { get; set; }
        public int StepWork { get; set; }
        public IList<EmployeeDetailsViewModel> Employees { get; set; }
        public CompanyDetailsViewModel Company { get; set; }

        public int SelectedCompanyId { get;set; }

    }
}