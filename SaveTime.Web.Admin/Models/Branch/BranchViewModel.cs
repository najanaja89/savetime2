﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveTime.Web.Admin.Models.Branch
{
    public class BranchViewModel
    {
        public IList<BranchDetailsViewModel> Branches { get; set; } = new List<BranchDetailsViewModel>();

        public CreateBranchViewModel CreateBranch { get; set; } = new CreateBranchViewModel();
    }
}