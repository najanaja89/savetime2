﻿using SaveTime.Web.Admin.Models.Account;
using SaveTime.Web.Admin.Models.Branch;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SaveTime.Web.Admin.Models
{
    public class CustomEmployeeViewModel
    {
        public int EmployeeId { get; set; }

        [Display(Name = "Работник")]
        public string EmployeeName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string BranchAddress { get; set; }
        public string SelectedBranchAddress { get; set; }
        public string SelectedBranchPhone { get; set; }
        public string SelectedAccountEmail { get; set; }
        public string SelectedAccountPhone { get; set; }
        public int AccountId { get; set; }
        public int BranchId { get; set; }
        public IList<BranchDetailsViewModel> Branches { get; set; }
        public IList<AccountDetailsViewModel> Accounts { get; set; }

        public override string ToString()
        {
            return $"{EmployeeName}: {Phone}, {Email}. {CompanyName} ({BranchAddress})";
        }
    }
}