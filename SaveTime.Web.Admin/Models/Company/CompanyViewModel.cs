﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveTime.Web.Admin.Models.Company
{
    public class CompanyViewModel
    {
        //for Company/Index
        public IList<CompanyDetailsViewModel> Companies { get; set; } = new List<CompanyDetailsViewModel>();

        //for Company/Create
        public CreateCompanyViewModel CreateCompany { get; set; } = new CreateCompanyViewModel();
    }
}