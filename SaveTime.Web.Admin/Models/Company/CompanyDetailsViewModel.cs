﻿using SaveTime.Web.Admin.Models.Branch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace SaveTime.Web.Admin.Models.Company
{
    public class CompanyDetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        [ScriptIgnore]
        public List<BranchDetailsViewModel> Branches { get; set; }

    }
}