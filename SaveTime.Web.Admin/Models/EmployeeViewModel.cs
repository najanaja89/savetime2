﻿using SaveTime.DataModel.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaveTime.Web.Admin.Models
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public string NameEmployee { get; set; }
        public string PhoneAccount { get; set; }
        public string EmailAccount { get; set; }
        public Account account { get; set; }
        public string NameCompany { get; set; }
        public string AddressBranch { get; set; }

    }
}