﻿using SaveTime.DataModel.Marker;

namespace SaveTime.DataModel.Dictionary
{
    public class Role : IEntity
    {
        public int Id { get; set; } // 1, 2
        public string Title { get; set; } // Admin, Master

    }
}
