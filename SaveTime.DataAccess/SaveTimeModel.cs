﻿namespace SaveTime.DataAccess
{
    using SaveTime.DataModel.Business;
    using SaveTime.DataModel.Dictionary;
    using SaveTime.DataModel.Organization;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class SaveTimeModel : DbContext
    {
        public SaveTimeModel()
            : base("name=SaveTimeModel")
        {
            Database.SetInitializer(new DataInitializer());
        }

        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Record> Records { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Employer> Employers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

        }


    }

}