﻿using SaveTime.DataModel.Organization;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveTime.DataAccess
{
    public class DataInitializer : DropCreateDatabaseAlways<SaveTimeModel>
    {
        protected override void Seed(SaveTimeModel context)
        {
            var accounts = new List<Account>
            {
                new Account { Email ="account1@email.com", Phone="888888888", Password="123"},
                new Account { Email ="account2@email.com", Phone="999999999", Password="123"},
                new Account { Email ="account3@email.com", Phone="777777777", Password="123"},
                new Account { Email ="account4@email.com", Phone="666666666", Password="123"},
            };

            var companies = new List<Company>
            {
                new Company{ City = "Nur-Sultan", Name = "TopGun Nur-Sultan" },
                new Company{ City = "Almaty", Name = "TopGun Almaty" }
            };

            var branches = new List<Branch>
            {
                new Branch { Address = "Republic 25b", Email="topgun@republic25b.kz", Phone="555333222111", StartWork=DateTime.Parse("09:00"), EndWork =DateTime.Parse("21:00"), StepWork = 1, Company = companies[0]},
                new Branch { Address = "Republic 24b", Email="topgun@republic24b.kz", Phone="333222111222", StartWork=DateTime.Parse("09:00"), EndWork =DateTime.Parse("21:00"), StepWork = 1, Company = companies[0]},
                new Branch { Address = "Republic 26b", Email="topgun@republic26b.kz", Phone="999666555222", StartWork=DateTime.Parse("09:00"), EndWork =DateTime.Parse("21:00"), StepWork = 1, Company = companies[0]},
                new Branch { Address = "Makatayeva 15", Email="topgun@Makatayeva15.kz", Phone="777766655222", StartWork=DateTime.Parse("09:00"), EndWork =DateTime.Parse("21:00"), StepWork = 1, Company = companies[1]},
            };

            context.Accounts.AddRange(accounts);
            context.Companies.AddRange(companies);
            context.Branches.AddRange(branches);
            context.SaveChanges();


            base.Seed(context);
        }
    }
}
